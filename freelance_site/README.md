# Freelance site Semester work


## Быстрый старт

1. Клонируйте репозиторий
        ```
        > git clone https://gitlab.com/Peter_Kurkin/cloud-cloud-rain.git
        ```
        ИЛИ
        ```
        > git clone git@gitlab.com:Peter_Kurkin/cloud-cloud-rain.git
        ```

```
pip install -r requirements.txt
```
- для установки нужных библиотек

1. Создать БД, зайдя в свой Postgres
    > psql -U (username)
    > 
    > CREATE DATABASE (name_database);
2. В local_configs.py указать путь до базы данных:
    > postgresql://(username):(password)@localhost:(localhost)/(name_database)

Чтобы запустить сервер через gunicorn, пропишите в терминале - ```sh gunicorn -b 127.0.0.1:5000 main:app ```
