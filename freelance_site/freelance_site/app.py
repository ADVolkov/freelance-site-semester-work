from flask import Flask

import os

from local_configs import Configuration
from database import db, migrate

app = Flask(__name__)

if not os.getenv('IS_PRODUCTION', None):
    app.config.from_object(Configuration)

db.init_app(app)
migrate.init_app(app, db)
