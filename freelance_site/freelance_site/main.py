from app import app
from error_handlers import page_not_found
from welcome.controllers import welcome_blueprint
from accounts.login_manager import login_manager
from accounts.controllers import accounts_blueprint
from order_manager.controllers import order_manager_blueprint

login_manager.init_app(app)

app.register_blueprint(order_manager_blueprint, url_prefix='/order_manager')
app.register_blueprint(accounts_blueprint, url_prefix='/accounts')
app.register_blueprint(welcome_blueprint, url_prefix='/')
app.register_error_handler(404, page_not_found)


if __name__ == '__main__':
    app.run()
