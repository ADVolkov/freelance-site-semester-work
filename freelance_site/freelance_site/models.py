from flask_login import UserMixin

from database import db


class UploadFile(UserMixin, db.Model):
    __tablename__ = "uploadfiles"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    data = db.Column(db.LargeBinary, nullable=False)

    user = db.relationship("User", backref='avatar', uselist=False)
    order_additional_file = db.relationship("Order", backref='additionalfile', uselist=False,
                                            foreign_keys='Order.additional_file_id')
    order_solution_file = db.relationship("Order", backref='solutionfile', uselist=False,
                                          foreign_keys='Order.solution_file_id')
