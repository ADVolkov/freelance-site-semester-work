from werkzeug.security import generate_password_hash

from database import db
from app import app


def main():
    with app.app_context():
        db.create_all()

        db.session.commit()


if __name__ == '__main__':
    main()
