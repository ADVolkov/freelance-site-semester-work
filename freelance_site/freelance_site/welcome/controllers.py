from flask import Blueprint, render_template, request, redirect, url_for

from welcome.forms import ContactForm
from welcome.services import add_connection_to_database

welcome_blueprint = Blueprint('welcome', __name__, template_folder='templates')


@welcome_blueprint.route('/', methods=('GET', 'POST'))
def main_page():
    form = ContactForm()
    if form.validate_on_submit():
        name = form.name.data
        email = form.email.data
        password = form.message.data
        add_connection_to_database(name, email, password)
        return redirect(url_for('welcome.main_page'))

    return render_template('welcome/main_page.html', form=form)
