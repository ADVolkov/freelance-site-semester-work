from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Email


class ContactForm(FlaskForm):
    name = StringField("Ваше имя: ", validators=[DataRequired(message='Поле обязательно')])
    email = StringField("Почта: ",
                        validators=[Email(message='Почта некорректна'), DataRequired(message='Поле обязательно')])
    message = TextAreaField("Сообщение:", validators=[DataRequired(message='Поле обязательно')])
    submit = SubmitField("Отправить")
