from welcome.models import Feedback

from database import db
from app import app


def add_connection_to_database(name: str, email: str, message: str) -> None:
    with app.app_context():
        db.create_all()
        db.session.add(Feedback(name=name, email=email, message=message))
        db.session.commit()
