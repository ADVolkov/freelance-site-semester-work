import pytest
from flask import url_for
from werkzeug.security import generate_password_hash

import main as app_module
from app import db
from accounts.models import User


@pytest.fixture
def app():
    app_module.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
    app_module.app.config['WTF_CSRF_ENABLED'] = False
    with app_module.app.app_context():
        db.create_all()
    yield app_module.app
    with app_module.app.app_context():
        db.session.remove()
        db.drop_all()


@pytest.fixture
def logged_in_client(client):
    db.session.add(
        User(username='nagibator228', email='test1@mail.ru', hashed_password=generate_password_hash('12345')))
    db.session.commit()
    client.post(url_for('accounts.login'), data=dict(username='nagibator228', email='test1@mail.ru', password='12345'))
    yield client
    client.get(url_for('accounts.logout'))
