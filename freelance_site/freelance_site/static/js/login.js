$(document).ready(function () {
    $("#form1").submit(function (event) {
        sendAjaxForm("form1", "warning");
        event.preventDefault();
    });
});

function sendAjaxForm(form_ajax, msg) {
    var form = $("#" + form_ajax);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function (response) {
            var json = jQuery.parseJSON(response);
            $('#' + msg).html(json.msg);
            if (json.success == 'false') {
                alert("Что-то пошло не так!");
                console.log("Ошибка");
            } else {
                window.location.replace("../order_manager/all_orders");
            }
        }
        ,
        error: function (error) {
            console.log(error);
        }
    });
}
