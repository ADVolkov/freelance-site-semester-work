from datetime import datetime

import pytest
from flask import url_for

from order_manager.models import Order


def test_main_page(logged_in_client):
    res = logged_in_client.get(url_for('order_manager.main_page'), follow_redirects=True)
    assert res == 200
    assert res.request.path == url_for('order_manager.all_orders')


@pytest.mark.parametrize('order_name,order_spec_id,order_type_id,deadline,description,price', [
    ('', 1, 1, '12.12.21', 'vfdvfd', 100),
    ('aaa', 's', '3', '12/12/21', 'vfdvd', 100),
    ('asa', 0, 0, 10, 'fgvfrd', '100'),
    ('asa', 0, 'g', 10 / 12 / 21, 'fgvfrd', 'gffd')
])
def test_create_invalid_post_work_should_not_create_new_post(logged_in_client, order_name, order_spec_id, order_type_id,
                                                             deadline, description, price):
    logged_in_client.post(url_for('order_manager.order_work'),
                          data=dict(order_name=order_name, order_spec_id=order_spec_id,
                                    order_type_id=order_type_id, deadline=deadline, description=description,
                                    price=price), follow_redirects=True)
    assert Order.query.count() == 0


@pytest.mark.parametrize('order_name,order_spec_id,order_type_id,deadline,description,price', [
    ('vrf', 1, 1, datetime(year=22, month=2, day=5), 'vfdvfd', 100),
    ('aaa', 2, 2, datetime(year=21, month=3, day=12), 'vfdvd', 100),
    ('asa', 3, 5, datetime(year=21, month=5, day=1), '100', 228),
    ('asa', 1, 9, datetime(year=21, month=12, day=1), 'fugnd', 1)
])
def test_create_post_should_create_new_post(logged_in_client, order_name, order_spec_id, order_type_id,
                                            deadline, description, price):
    logged_in_client.post(url_for('company_management.create_department_page'),
                          data=dict(order_name=order_name, order_spec_id=order_spec_id,
                                    order_type_id=order_type_id, deadline=deadline, description=description,
                                    price=price),
                          follow_redirects=True)
    assert Order.query.count() == 1
    assert Order.query.filter_by(order_name=order_name, order_spec_id=order_spec_id, order_type_id=order_type_id,
                                 deadline=deadline, description=description, price=price).count() == 1


@pytest.mark.parametrize('price', [
    '1fd', 'frfrefdc', '11112434f3'
])
def test_create_post_with_invalid_price_should_redirect(logged_in_client, price):
    res = logged_in_client.post(url_for('order_manager.order_work'), follow_redirects=True)
    assert res == 200
    assert res.request.path == url_for('order_manager.order_work')
