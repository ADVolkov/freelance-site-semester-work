from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed
from wtforms import StringField, SubmitField, IntegerField, FileField, DateField, SelectField, FloatField, \
    PasswordField, TextAreaField
from wtforms.validators import DataRequired, NumberRange, Email

from order_manager.custom_select_field import ExtendedSelectField
from order_manager.services import choice_specializations, choice_types_of_order


class PostOrderForm(FlaskForm):
    name = StringField('Название работы: ', validators=[DataRequired(message='Поле обязательно')])
    specialization = ExtendedSelectField('Специализация работы:',
                                         choices=choice_specializations(),
                                         validators=[DataRequired(message='Поле обязательно')])
    type = SelectField('Тип работы:', choices=choice_types_of_order(),
                       validators=[DataRequired(message='Поле обязательно')])
    file = FileField('Дополнительный файл:')
    deadline = DateField('Срок работы:', format='%d/%m/%y', validators=[DataRequired(message='Поле обязательно')])
    description = StringField('Опишите ваше работу:')
    price = IntegerField('Цена работы: ',
                         validators=[DataRequired(message='Поле обязательно'),
                                     NumberRange(min=0, max=100000,
                                                 message='Цена некорректна или превышает лимит')])
    submit = SubmitField('Опубликовать')


class BalanceForm(FlaskForm):
    refill = FloatField('Пополнение счета',
                        validators=[DataRequired(),
                                    NumberRange(min=0, max=100000, message='Цена некорректна или превышает лимит')])
    submit = SubmitField('Пополнить')


class ChangePasswordForm(FlaskForm):
    password = StringField('Старый пароль', validators=[DataRequired()])
    new_password = PasswordField('Новый пароль', validators=[DataRequired()])
    rep_new_password = PasswordField('Повторите новый пароль', validators=[DataRequired()])
    submit = SubmitField('Изменить')


class ChangeLoginForm(FlaskForm):
    username = StringField('Имя пользователя: ', validators=[DataRequired(message='Поле обязатльно')])
    submit = SubmitField('Изменить')


class ChangeEmailForm(FlaskForm):
    email = StringField('Почта: ',
                        validators=[DataRequired(message='Поле обязательно'), Email(message='Почта некорректна')])
    submit = SubmitField('Изменить')


class ChangeUserAvatarForm(FlaskForm):
    avatar = FileField('Загрузить новое фото:', validators=[DataRequired(), FileAllowed(['jpg', 'jpeg', 'png'])])
    submit = SubmitField('Установить новое фото')


class ChangeDescriptionForm(FlaskForm):
    description = TextAreaField('Добавьте информацию о себе:', validators=[DataRequired()])
    submit = SubmitField('Сохранить')


class TakeOrderForm(FlaskForm):
    offer = TextAreaField()
    submit_take_order = SubmitField('Предложить себя')


class CompleteOrderForm(FlaskForm):
    solution_file = FileField('Файл с решениями: ', validators=[DataRequired()])
    submit = SubmitField('Отправить на проверку')


class OrderAcceptanceForm(FlaskForm):
    submit_accept_order = SubmitField('Принять работу')
