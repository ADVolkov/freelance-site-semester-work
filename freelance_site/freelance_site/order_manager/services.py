from flask_login import current_user
from werkzeug.utils import secure_filename

from app import app
from database import db
from models import UploadFile
from order_manager.models import SpecializationType, OrderType, Order, OrderSpecialization


def get_all_specializationtypes():
    return SpecializationType.query.all()


def choice_specializations() -> []:
    with app.app_context():
        db.create_all()
        return [(spec_type.name, tuple((spec.id, spec.name) for spec in spec_type.specializations)) for spec_type in
                get_all_specializationtypes()]


def choice_types_of_order() -> []:
    with app.app_context():
        db.create_all()
        return [(order_type.id, order_type.name) for order_type in get_all_order_types()]


def get_all_specializations():
    return OrderSpecialization.query.all()


def get_all_order_types():
    return OrderType.query.all()


def get_published_orders_by_specialization(spec_id: int):
    if spec_id != 0:
        return Order.query.filter_by(specialization_id=spec_id, status='Опубликовано').filter(
            Order.customer_id != current_user.id).order_by(
            Order.deadline)
    return Order.query.filter_by(status='Опубликовано').filter(
        Order.customer_id != current_user.id).order_by(Order.deadline)


def get_order_by_id(id: int):
    return Order.query.get(id)


def get_my_orders_by_status(status: str):
    return current_user.my_orders.filter_by(status=status)


def get_my_accepted_orders_by_id(status: str):
    return current_user.accepted_orders.filter_by(status=status)


def add_new_order_to_database(order_name, order_spec_id, order_type_id, file, deadline, description, price,
                              status='Опубликовано') -> None:
    new_order = Order(name=order_name, specialization_id=order_spec_id, type_id=order_type_id,
                      description=description, deadline=deadline, status=status, price=price,
                      customer_id=current_user.id)
    with app.app_context():
        if file is not None:
            new_file = UploadFile(name=file.filename, data=file.stream.read())
            new_order.additionalfile = new_file
            db.session.add(new_file)
        current_user.balance -= price
        db.session.add(new_order)
        db.session.commit()


def add_solution_file_and_put_on_check(file, order_id: int):
    with app.app_context():
        order = Order.query.get(order_id)
        new_file = UploadFile(name=file.filename, data=file.stream.read())
        order.solutionfile = new_file
        order.status = 'На проверке'
        db.session.add(new_file)
        db.session.commit()
