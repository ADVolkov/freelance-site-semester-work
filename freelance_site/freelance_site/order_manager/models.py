from base64 import b64encode

from flask_login import UserMixin

from database import db
from models import UploadFile


class SpecializationType(UserMixin, db.Model):
    __tablename__ = "specializationtypes"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)

    specializations = db.relationship('OrderSpecialization', backref='specializationtype')


class OrderSpecialization(UserMixin, db.Model):
    __tablename__ = "orderspecializations"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    type_id = db.Column(db.Integer, db.ForeignKey('specializationtypes.id'), nullable=False)
    orders = db.relationship('Order', backref='orderspecialization', lazy='dynamic')


class OrderType(UserMixin, db.Model):
    __tablename__ = "ordertypes"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    orders = db.relationship('Order', backref='ordertype', lazy='dynamic')


class Review(UserMixin, db.Model):
    __tablename__ = "reviews"
    __table_args__ = (
        db.CheckConstraint('grade in (1, 2, 3, 4, 5)'),
    )

    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(300))
    grade = db.Column(db.Integer, nullable=False)
    order = db.relationship('Order', backref='review', uselist=False)


class Order(UserMixin, db.Model):
    __tablename__ = "orders"
    __table_args__ = (
        db.CheckConstraint('price >= 0'),
    )

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    deadline = db.Column(db.Date, nullable=False)
    status = db.Column(db.String(20), nullable=False)
    price = db.Column(db.Integer, nullable=False)
    description = db.Column(db.String(300))
    specialization_id = db.Column(db.Integer, db.ForeignKey('orderspecializations.id'), nullable=False)
    type_id = db.Column(db.Integer, db.ForeignKey('ordertypes.id'), nullable=False)
    additional_file_id = db.Column(db.Integer, db.ForeignKey('uploadfiles.id'), unique=True)
    solution_file_id = db.Column(db.Integer, db.ForeignKey('uploadfiles.id'), unique=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    executor_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    review_id = db.Column(db.Integer, db.ForeignKey('reviews.id'), unique=True)
    messages = db.relationship('ChatMessages', backref='order', lazy='dynamic')

    def get_file(self):
        return b64encode(UploadFile.query.get(self.additional_file_id).data).decode("utf-8")

    def get_solution_file(self):
        return b64encode(UploadFile.query.get(self.solution_file_id).data).decode("utf-8")

    def __repr__(self):
        return f"<Order {self.order_id}>"


class ChatMessages(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    order_id = db.Column(db.Integer, db.ForeignKey('orders.id'), nullable=False)
    message = db.Column(db.String, nullable=False)
    public_date = db.Column(db.Date, nullable=False)
