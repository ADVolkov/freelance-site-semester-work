from flask import redirect, url_for, render_template, Blueprint
from flask_login import current_user
from werkzeug.security import check_password_hash

from accounts.send_message_confirm_email import send_link_change_email
from accounts.services import change_current_user_password, change_current_user_avatar, change_current_user_describe, \
    add_current_user_to_order_executor, get_user_by_id, increase_current_user_balance, find_user_by_username, \
    change_current_user_login, find_user_by_email, complete_order
from local_configs import Configuration
from order_manager.forms import PostOrderForm, BalanceForm, ChangePasswordForm, ChangeLoginForm, ChangeEmailForm, \
    ChangeUserAvatarForm, ChangeDescriptionForm, TakeOrderForm, CompleteOrderForm, OrderAcceptanceForm
from order_manager.services import get_all_specializationtypes, get_all_order_types, \
    get_published_orders_by_specialization, \
    get_order_by_id, get_my_orders_by_status, get_my_accepted_orders_by_id, add_new_order_to_database, \
    add_solution_file_and_put_on_check

order_manager_blueprint = Blueprint('order_manager', __name__, template_folder='templates')


@order_manager_blueprint.route('/', methods=('GET', 'POST'))
def main_page():
    return redirect(url_for('order_manager.all_orders'))


@order_manager_blueprint.route('/order_work', methods=('GET', 'POST'))
def order_work():
    form = PostOrderForm()
    if form.validate_on_submit():
        order_name = form.name.data
        order_spec_id = form.specialization.data
        order_type_id = form.type.data
        file = form.file.data
        deadline = form.deadline.data
        description = form.description.data
        try:
            price = int(form.price.data)
        except:
            print('Введена некорректная цена')
            return redirect(url_for('order_manager.order_work'))
        else:
            if current_user.balance >= price:
                add_new_order_to_database(order_name, order_spec_id, order_type_id, file, deadline, description, price)
                print('Заказ успешно добавлен')
                return redirect(url_for('order_manager.all_orders'))
            else:
                print('У вас недостаточно денег')
                return redirect(url_for('order_manager.order_work'))

    return render_template('order_manager/post_work.html', specializationtypes=get_all_specializationtypes(),
                           types=get_all_order_types(), form=form)


@order_manager_blueprint.route('/balance', methods=('GET', 'POST'))
def balance():
    form = BalanceForm()
    if form.validate_on_submit():
        try:
            sum_of_money = float(form.refill.data)
        except:
            print('==========Введена некорректная сумма==========')
            return redirect(url_for('order_manager.balance'))
        else:
            print('==========Счет успешно пополнен==========')
            increase_current_user_balance(sum_of_money)
            return redirect(url_for('order_manager.balance'))
    return render_template('order_manager/balance.html', specializationtypes=get_all_specializationtypes(), form=form)


@order_manager_blueprint.route('/all_orders', methods=('GET', 'POST'))
@order_manager_blueprint.route('/all_orders/<int:spec_id>/<int:page>', methods=('GET', 'POST'))
def all_orders(spec_id=0, page=1):
    orders = get_published_orders_by_specialization(spec_id=spec_id).paginate(page, Configuration.POSTS_PER_PAGE, False)
    return render_template('order_manager/all_orders.html', orders=orders, cur_page=page,
                           count=len(get_published_orders_by_specialization(
                               spec_id=spec_id).all()) // Configuration.POSTS_PER_PAGE,
                           specializationtypes=get_all_specializationtypes(), current_spec_id=spec_id)


@order_manager_blueprint.route("/order_page/<int:order_id>", methods=("GET", "POST"))
def order_page(order_id: int):
    take_order_form = TakeOrderForm()
    complete_order_form = CompleteOrderForm()
    order_acceptance_form = OrderAcceptanceForm()
    if complete_order_form.validate_on_submit() and complete_order_form.solution_file.data:
        solution_file = complete_order_form.solution_file.data
        add_solution_file_and_put_on_check(solution_file, order_id)
        print('Заказ перешел в стадию проверки')
        return redirect(url_for('order_manager.order_page', order_id=order_id))
    elif take_order_form.validate_on_submit() and take_order_form.submit_take_order.data:
        add_current_user_to_order_executor(order_id)
        print('Заказ принят')
        return redirect(url_for('order_manager.order_page', order_id=order_id))
    elif order_acceptance_form.validate_on_submit() and order_acceptance_form.submit_accept_order.data:
        complete_order(order_id)
        return redirect(url_for('order_manager.order_page', order_id=order_id))
    try:
        order = get_order_by_id(id=order_id)
        customer = get_user_by_id(user_id=order.customer_id)
        executor = get_user_by_id(user_id=order.executor_id)
        return render_template('order_manager/order_page.html', order=order, customer=customer, executor=executor,
                               specializationtypes=get_all_specializationtypes(), take_order_form=take_order_form,
                               complete_order_form=complete_order_form, order_acceptance_form=order_acceptance_form)
    except:
        return redirect(url_for('order_manager.all_orders'))


@order_manager_blueprint.route('/profile', methods=('GET', 'POST'))
def profile():
    form = ChangeDescriptionForm()
    if form.validate_on_submit():
        info_about_user = form.description.data
        change_current_user_describe(info_about_user)
        return redirect(url_for('order_manager.profile'))

    return render_template('order_manager/profile.html', specializationtypes=get_all_specializationtypes(), form=form)


@order_manager_blueprint.route("/user/<int:user_id>", methods=["GET"])
def user_profile(user_id: int):
    try:
        user = get_user_by_id(user_id=user_id)
        return render_template('order_manager/user_profile.html', user=user,
                               specializationtypes=get_all_specializationtypes())
    except:
        return redirect(url_for('order_manager.all_orders'))


@order_manager_blueprint.route('/profile/change_description', methods=('GET', 'POST'))
def profile_change_description():
    form = ChangeDescriptionForm()
    if form.validate_on_submit():
        info_about_user = form.description.data
        change_current_user_describe(info_about_user)
        return redirect(url_for('order_manager.profile'))

    return render_template('order_manager/profile_change_description.html',
                           specializationtypes=get_all_specializationtypes(), form=form)


@order_manager_blueprint.route('/my_orders', methods=('GET', 'POST'))
@order_manager_blueprint.route('/my_orders/<string:status>/<int:page>', methods=('GET', 'POST'))
def my_orders(status='Опубликовано', page=1):
    orders = get_my_orders_by_status(status=status).paginate(page, Configuration.POSTS_PER_PAGE, False)
    return render_template('order_manager/my_orders.html', orders=orders, cur_page=page,
                           count=len(get_my_orders_by_status(status=status).all()) // Configuration.POSTS_PER_PAGE,
                           status=status,
                           specializationtypes=get_all_specializationtypes())


@order_manager_blueprint.route('/taken_orders', methods=('GET', 'POST'))
@order_manager_blueprint.route('/taken_orders/<string:status>/<int:page>', methods=('GET', 'POST'))
def taken_orders(status='На выполнении', page=1):
    orders = get_my_accepted_orders_by_id(status=status).paginate(page, Configuration.POSTS_PER_PAGE, False)
    return render_template('order_manager/taken_orders.html', orders=orders, cur_page=page,
                           count=len(get_my_accepted_orders_by_id(status=status).all()) // Configuration.POSTS_PER_PAGE,
                           status=status,
                           specializationtypes=get_all_specializationtypes())


@order_manager_blueprint.route('/preferences', methods=('GET', 'POST'))
def preferences():
    form = ChangeUserAvatarForm()
    if form.validate_on_submit():
        file = form.avatar.data
        print(type(file))
        if file.content_type in ('image/jpeg', 'image/png', 'image/svg+xml'):
            change_current_user_avatar(file)
            print('Фото профиля успешно изменено')
            return redirect(url_for('order_manager.preferences'))
        else:
            print('Некорректный тип файла')
            return redirect(url_for('order_manager.preferences'))

    return render_template('order_manager/preferences.html', specializationtypes=get_all_specializationtypes(),
                           form=form)


@order_manager_blueprint.route('/preferences/change_email', methods=('GET', 'POST'))
def change_email():
    form = ChangeEmailForm()
    if form.validate_on_submit():
        new_email = form.email.data
        if find_user_by_email(new_email):
            print(find_user_by_email(new_email))
            print('Данная почта уже занята')
            return redirect(url_for('order_manager.change_email'))
        send_link_change_email(new_email)
        print('Вам на почту было отправлено сообщение с подтверждением')
        return redirect(url_for('order_manager.preferences'))
    return render_template('order_manager/change_email.html', specializationtypes=get_all_specializationtypes(),
                           form=form)


@order_manager_blueprint.route('/preferences/change_login', methods=('GET', 'POST'))
def change_login():
    form = ChangeLoginForm()
    if form.validate_on_submit():
        new_username = form.username.data
        if find_user_by_username(new_username):
            print('Данный логин уже занят')
            return redirect(url_for('order_manager.change_login'))
        change_current_user_login(new_username)
        print('Логин успешно изменен')
        return redirect(url_for('order_manager.preferences'))
    return render_template('order_manager/change_login.html', specializationtypes=get_all_specializationtypes(),
                           form=form)


@order_manager_blueprint.route('/preferences/change_password', methods=('GET', 'POST'))
def change_password():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        password = form.password.data
        new_password = form.new_password.data
        rep_new_password = form.rep_new_password.data
        if new_password != rep_new_password:
            return redirect(url_for('order_manager.change_password'))
        if not check_password_hash(current_user.hashed_password, password):
            return redirect(url_for('order_manager.change_password'))

        change_current_user_password(new_password)
        return redirect(url_for('order_manager.preferences'))

    return render_template('order_manager/change_password.html', specializationtypes=get_all_specializationtypes(),
                           form=form)
