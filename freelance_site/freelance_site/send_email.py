from flask_mail import Mail, Message
from app import app


def send_email(header, emails: list, body):
    with app.app_context():
        mail = Mail(app)
        msg = Message(header, recipients=emails)
        msg.html = body
        mail.send(msg)
