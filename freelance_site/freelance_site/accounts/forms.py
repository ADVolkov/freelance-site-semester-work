from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, BooleanField
from wtforms.validators import DataRequired, Email


class RegisterForm(FlaskForm):
    username = StringField('Имя пользователя: ', validators=[DataRequired(message='Поле обязательно')])
    email = StringField('Почта: ',
                        validators=[DataRequired(message='Поле обязательно'), Email(message='Почта некорректна')])
    password = PasswordField('Пароль: ', validators=[DataRequired(message='Поле обязательно')])
    repeat_password = PasswordField('Повторите пароль: ', validators=[DataRequired(message='Поле обязательно')])
    submit = SubmitField("Зарегистрироваться")


class LoginForm(FlaskForm):
    username = StringField('Имя пользователя: ', validators=[DataRequired(message='Поле обязатльно')])
    password = PasswordField('Пароль: ', validators=[DataRequired(message='Поле обязательно')])
    remember_me = BooleanField('Запомнить меня')
    submit = SubmitField('Войти')
