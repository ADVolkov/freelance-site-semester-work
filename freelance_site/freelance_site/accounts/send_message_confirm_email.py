from flask import render_template
from flask_login import current_user

from send_email import send_email
from local_configs import Configuration

import jwt
from time import time


def form_mail_token(username='', email='', password='', expires_in=600) -> str:
    return jwt.encode(
        {'confirm_mail': f'{username}:+:{email}:+:{password}', 'exp': time() + expires_in}, Configuration.SECRET_KEY,
        algorithm='HS256')


def verify_mail_token_new_user(token: str) -> ():
    try:
        username, email, password = jwt.decode(token, Configuration.SECRET_KEY, algorithms=['HS256'])[
            'confirm_mail'].split(':+:')
    except:
        pass
    else:
        return username, email, password


def verify_mail_token_current_user(token: str):
    try:
        email = jwt.decode(token, Configuration.SECRET_KEY, algorithms=['HS256'])['confirm_mail'].split(':+:')[1]
    except:
        pass
    else:
        return email


def send_link_confirm_email(username: str, email: str, password: str):
    token = form_mail_token(username, email, password)
    send_email('Подтверждение почты',
               emails=[email],
               body=render_template('accounts/confirm_email_new_user.html', token=token,
                                    username=username))


def send_link_change_email(email: str):
    token = form_mail_token(email=email)
    send_email('Подтверждение почты',
               emails=[email],
               body=render_template('accounts/confirm_email_current_user.html', token=token,
                                    username=current_user.username))
