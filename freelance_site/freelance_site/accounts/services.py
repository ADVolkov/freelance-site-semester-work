from flask_login import current_user
from sqlalchemy import text
from werkzeug.security import check_password_hash, generate_password_hash

from models import UploadFile
from accounts.models import User
from database import db
from app import app
from order_manager.models import Order


class NotFoundError(Exception):
    pass


class IncorrectPasswordError(Exception):
    pass


def find_user_by_username_and_password(username: str, password: str):
    user = User.query.filter_by(username=username).first()
    if not user:
        raise NotFoundError('Incorrect username')
    if not check_password_hash(user.hashed_password, password):
        raise IncorrectPasswordError('Incorrect password')
    return user


def find_user_by_username(username: str):
    return User.query.filter_by(username=username).first()


def find_user_by_email(email: str):
    user = db.engine.execute(text(f"SELECT * FROM users WHERE email = '{email}'"))
    return user


def find_user_by_id(user_id: int):
    user = User.query.filter_by(id=user_id).first()
    if not user:
        raise NotFoundError()
    return user


def get_user_by_id(user_id: int):
    return User.query.get(user_id)


def add_new_user_to_database(username: str, email: str, password: str) -> None:
    with app.app_context():
        db.create_all()
        db.session.add(User(email=email, username=username, hashed_password=generate_password_hash(password)))
        db.session.commit()


def change_current_user_login(new_username: str):
    with app.app_context():
        current_user.username = new_username
        db.session.commit()


def change_current_user_email(new_email: str):
    with app.app_context():
        current_user.email = new_email
        db.session.commit()


def change_current_user_password(new_password: str):
    with app.app_context():
        current_user.hashed_password = generate_password_hash(new_password)
        db.session.commit()


def change_current_user_avatar(file) -> None:
    with app.app_context():
        current_file = UploadFile.query.get(current_user.avatar_file_id)
        if current_file:
            current_file.name = file.filename
            current_file.data = file.stream.read()
            current_user.avatar = current_file
            db.session.commit()
        else:
            new_file = UploadFile(name=file.filename, data=file.stream.read())
            current_user.avatar = new_file
            db.session.add(new_file)
            db.session.commit()


def change_current_user_describe(describe: str) -> None:
    with app.app_context():
        current_user.info = describe
        db.session.commit()


def add_current_user_to_order_executor(order_id: int) -> None:
    order = Order.query.get(order_id)
    if order.executor_id is None:
        with app.app_context():
            order.executor = current_user
            order.status = 'На выполнении'
            db.session.commit()


def increase_current_user_balance(sum_of_money: float) -> None:
    with app.app_context():
        current_user.balance += sum_of_money
        db.session.commit()


def complete_order(order_id):
    with app.app_context():
        order = Order.query.get(order_id)
        order.status = 'Выполнено'
        executor = User.query.get(order.executor_id)
        executor.balance += order.price
        db.session.commit()
