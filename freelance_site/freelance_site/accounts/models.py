from flask_login import UserMixin

from database import db
from models import UploadFile
# noinspection PyUnresolvedReferences
from order_manager.models import Order, ChatMessages

from base64 import b64encode


class User(UserMixin, db.Model):
    __tablename__ = "users"
    __table_args__ = (
        db.CheckConstraint('rate <= 5'),
        db.CheckConstraint('rate > 0'),
    )

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), index=True, unique=True, nullable=False)
    email = db.Column(db.String(100), unique=True, nullable=False)
    hashed_password = db.Column(db.String, unique=False, nullable=False)
    avatar_file_id = db.Column(db.Integer, db.ForeignKey('uploadfiles.id'), unique=True)
    balance = db.Column(db.Float, nullable=False, default=0)
    info = db.Column(db.String(1000))
    rate = db.Column(db.Float)

    my_orders = db.relationship('Order', backref='customer', lazy='dynamic', foreign_keys='Order.customer_id')
    accepted_orders = db.relationship('Order', backref='executor', lazy='dynamic',
                                      foreign_keys='Order.executor_id')
    messages = db.relationship('ChatMessages', backref='user', lazy='dynamic')

    def get_avatar(self):
        return b64encode(UploadFile.query.get(self.avatar_file_id).data).decode("utf-8")

    def __repr__(self):
        return f"<User {self.username}>"
