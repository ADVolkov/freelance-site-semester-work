import json

from flask import request, redirect, url_for, flash, render_template, Blueprint, jsonify
from flask_login import login_user, login_required, logout_user, current_user

from accounts.forms import RegisterForm, LoginForm
from accounts.services import find_user_by_username_and_password, add_new_user_to_database, find_user_by_username, \
    find_user_by_email, change_current_user_email
from accounts.send_message_confirm_email import verify_mail_token_new_user, send_link_confirm_email, \
    verify_mail_token_current_user

accounts_blueprint = Blueprint('accounts', __name__, template_folder='templates')


@accounts_blueprint.route('/verify_email_new_user/<token>', methods=['GET'])
def verify_email_new_user(token):
    if current_user.is_authenticated:
        return redirect(url_for('welcome.main_page'))
    username, email, password = verify_mail_token_new_user(token)
    if find_user_by_username(username):
        print('Почта уже подтверждена')
    else:
        add_new_user_to_database(username, email, password)
    return redirect(url_for('welcome.main_page'))


@accounts_blueprint.route('/verify_email_current_user/<token>', methods=('GET', 'POST'))
def verify_email_current_user(token):
    email = verify_mail_token_current_user(token)
    change_current_user_email(email)
    return 'Ваша почта успешно подтверждена'


@accounts_blueprint.route('/register', methods=('GET', 'POST'))
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        username = form.username.data
        email = form.email.data
        password = form.password.data
        repeat_password = form.repeat_password.data
        if password != repeat_password:
            print('Пароли не совпадают')
            return redirect(url_for('accounts.register'))
        if find_user_by_email(email):
            print('Пользователь с такой почтой уже существует')
            return redirect(url_for('accounts.register'))
        if find_user_by_username(username):
            print('Пользователь с таким логином уже существует')
            return redirect(url_for('accounts.register'))
        else:
            send_link_confirm_email(username, email, password)
            print('Вам на почту было отправлено сообщение с подтверждением')
            return redirect(url_for('welcome.main_page'))

    return render_template('accounts/register.html', form=form)


@accounts_blueprint.route('/login', methods=('GET', 'POST'))
def login():
    form = LoginForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        remember_me = form.remember_me.data
        try:
            user = find_user_by_username_and_password(username, password)
        except:
            return json.dumps({'success': 'false', 'msg': 'Пользователя с таким логином или паролем не существует'})
        else:
            login_user(user, remember=remember_me)
            return json.dumps({'success': 'true', 'msg': 'Вы успешно вошли'})
    return render_template('accounts/login.html', form=form)


@accounts_blueprint.route("/logout", methods=["GET"])
@login_required
def logout():
    logout_user()
    return redirect(url_for('welcome.main_page'))
